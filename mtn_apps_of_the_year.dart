// Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012;

void main(List<String> args) {
  var apps_of_the_year = [
    {"year": "2012", "name": "FNB Banking app"},
    {"year": "2013", "name": "SnapScan"},
    {"year": "2014", "name": "LIVE Inspect"},
    {"year": "2015", "name": "WumDrop"},
    {"year": "2016", "name": "Domestly"},
    {"year": "2017", "name": "Standard Bank's Shyft"},
    {"year": "2018", "name": "Khula eco-system"},
    {"year": "2019", "name": "Naked Insurance"},
    {"year": "2020", "name": "EasyEquities"},
    {"year": "2021", "name": "Edtech Ambani"},
  ];

// a) Sort and print the apps by name;
  apps_of_the_year.sort((a, b) {
    return a['name'].toString().compareTo(b['name'].toString());
  });
  print(apps_of_the_year.map((app) => app["name"]));

// b) Print the winning app of 2017 and the winning app of 2018.;
  var winning_app_2017_and_2018 = apps_of_the_year
      .where((app) => app["year"] == "2017" || app["year"] == "2018");
  print(winning_app_2017_and_2018);

// c) the Print total number of apps from the array.
  var total_apps = apps_of_the_year.length;
  print("Total number of apps in the array is $total_apps");
}
