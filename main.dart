// Write a basic program that stores and then prints the following data: Your name, favorite app, and city;

void main(List<String> args) {
  String name = "Louisa";
  String favorite_app = "Google";
  String city = "Johanesberg";

  print("My name is $name. My favorite app is $favorite_app. My city is $city");
}
