// 3. Create a class and
// a) then use an object to print the name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards.

class App {
  String? name;
  String? category;
  String? developer;
  String? year;

  App(this.name, this.category, this.developer, this.year);

  void printApp() {
    print("$name, $category, $developer, $year");
  }

  //b) Create a function inside the class, transform the app name to all capital letters and then print the output.

  void Caps() {
    print(name?.toUpperCase());
  }
}

void main(List<String> args) {
  var fnb = App("FNB Banking app", "Banking", "FNB", "2012");
  fnb.printApp();

  fnb.Caps();
}
